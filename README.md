# David's Plone 4.3 Buildout #

This is a basic Plone 4.3 buildout which includes some extra development tools

### Details ###
* Plone version 4.3.1
* Includes ipzope; a great help when developing

### How do I get set up? ###

Enter the plone directory

Create a virtual environment
```
#!python
virtualenv . 
```

Activate the environment
```
#!python
source bin/activate
```

Create a buildout.cfg file with the following:
```
#!python
[buildout]
extends =
    devsite.cfg
```

Run the bootstrap
```
#!python
python bootstrap.py
```

Run the buildout

```
#!python
buildout
```
Make sure it runs

```
#!python
instance fg
```
